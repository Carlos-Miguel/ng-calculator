import { Component } from '@angular/core';
import { parse } from 'ts-node';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TP Angular 1 x Miage Calculator';

  public op1 = 0;
  public op2 = 0;
  public res: number;
  public res2: number;
  public nboperation = 0;
  public listeMemo: Array<string>;


  constructor() {
    this.listeMemo = new Array<string>();
  }

  add() {
    this.res = this.op1 + this.op2;
    if (this.listeMemo.length == 10) {
      this.listeMemo.shift();
    }
    this.listeMemo.push((this.op1 + '+' + this.op2 + '=' + this.res));
    this.oper();

  }
  soustra() {
    this.res = this.op1 - this.op2;
    if (this.listeMemo.length == 10) {
      this.listeMemo.shift();
    }
    this.listeMemo.push((this.op1 + '-' + this.op2 + '=' + this.res));
    this.oper();
  }
  multi() {
    this.res = this.op1 * this.op2;
    if (this.listeMemo.length == 10) {
      this.listeMemo.shift();
    }
    this.listeMemo.push((this.op1 + '*' + this.op2 + '=' + this.res));
    this.oper();
  }
  divi() {
    this.res = this.op1 / this.op2;
    if (this.listeMemo.length == 10) {
      this.listeMemo.shift();
    }
    this.listeMemo.push((this.op1 + '/' + this.op2 + '=' + this.res));
    this.oper();
  }
  sauve() {
    this.res2 = this.res;
  }
  restaure() {
    this.op1 = this.res2;
  }

  oper() {
    if (this.nboperation < 10) {
      this.nboperation += 1;
    }
  }
  ac() {
    this.op2 = 0;
    this.op1 = 0;
    this.res = 0;
    this.res2 = 0;
    this.nboperation = 0;
  }
  choix(value: any) {
    const x = '' + this.op1 + value;
    this.op1 = parseFloat(x);
  }

  premierOp(l: string) {
    if (l.includes('+')) {
      return l.split('+')[0];
    }
    if (l.includes('/')) {
      return l.split('/')[0];
    }
    if (l.includes('*')) {
      return l.split('*')[0];
    }
    else {
      return l.split('-')[0];

    }
  }
  deuxiemeOp(l: string) {
    if (l.includes('+')) {
      let l1 = l.split('+')[1];
      return l1.split('=')[0];
    }
    if (l.includes('/')) {
      let l1 = l.split('/')[1];
      return l1.split('=')[0];
    }
    if (l.includes('*')) {
      let l1 = l.split('*')[1];
      return l1.split('=')[0];
    }
    else {
      let l1 = l.split('=')[0];
      return l1.split('-').slice(-1)[0];
    }

  }
  resultat(l) {
    return l.split('=')[1];
  }

  sauve2(l) {
    this.res = parseFloat(this.resultat(l));
    this.op1 = parseFloat(this.premierOp(l));
    this.op2 = parseFloat(this.deuxiemeOp(l));
  }
  delete(index) {
    if (index >= 0 && index < this.listeMemo.length) {
      this.listeMemo.splice(index, 1);
    }
  }

}
